use gst::glib;
use gst::prelude::*;

mod simple_gl_transform;

fn plugin_init(plugin: &gst::Plugin) -> Result<(), glib::BoolError> {
    gst::Element::register(
        Some(plugin),
        "simplegltransform",
        gst::Rank::None,
        simple_gl_transform::SimpleGLTransform::static_type(),
    )
}

gst::plugin_define!(
    gltransform_rs,
    "Plugin description goes here",
    plugin_init,
    "0.0.1",
    "unknown",
    "myplugin",
    "mypackage",
    "myrepo",
    "2023-06-17"
);
