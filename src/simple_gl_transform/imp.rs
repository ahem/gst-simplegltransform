use std::str::FromStr;
use std::sync::Mutex;

use gl_constants::*;
use gl_struct_loader::GlFns;
use gl_types::{GLboolean, GLfloat, GLuint, GLushort};
use gst::subclass::{prelude::*, ElementMetadata};
use gst::{
    glib, loggable_error, Buffer, BufferRef, Caps, FlowError, FlowSuccess, LoggableError,
    PadDirection, PadPresence, PadTemplate,
};
use gst_base::subclass::prelude::*;
use gst_gl::gl_video_frame::{ReadableGL, WritableGL};
use gst_gl::prelude::*;
use gst_gl::subclass::prelude::*;
use gst_gl::{GLContext, GLFramebuffer, GLShader, GLSyncMeta};
use gst_video::gst_base::subclass::BaseTransformMode;
use gst_video::video_frame::VideoFrame;
use gst_video::VideoInfo;
use once_cell::sync::Lazy;

use super::CAT;

struct State {
    gl: Box<GlFns>,

    shader: GLShader,
    fbo: GLFramebuffer,
    in_info: VideoInfo,
    out_info: VideoInfo,

    vertex_array_object: GLuint,
    vertex_buffer: GLuint,
    vbo_indices: GLuint,
}

pub struct SimpleGLTransform {
    state: Mutex<Option<State>>,
}

impl GLBaseFilterImpl for SimpleGLTransform {
    fn gl_set_caps(&self, incaps: &Caps, outcaps: &Caps) -> Result<(), LoggableError> {
        let context = GLContext::current().ok_or(loggable_error!(CAT, "unable to get context"))?;

        let shader = GLShader::new_default(&context)
            .map_err(|e| loggable_error!(CAT, format!("error creating shader {e}")))?;

        let in_info = VideoInfo::from_caps(incaps).map_err(LoggableError::from)?;
        let out_info = VideoInfo::from_caps(outcaps).map_err(LoggableError::from)?;

        let fbo = GLFramebuffer::with_default_depth(&context, out_info.width(), out_info.height());

        let mut gl = GlFns::new_boxed();
        unsafe {
            gl.load(|name| {
                let name = std::ffi::CStr::from_ptr(name.cast()).to_str().unwrap();
                context.proc_address(name) as *const std::ffi::c_void
            })
        };

        let mut vertex_array_object: GLuint = 0;
        let mut vertex_buffer: GLuint = 0;
        let mut vbo_indices: GLuint = 0;

        unsafe {
            gl.GenVertexArrays(1, &mut vertex_array_object as _);
            gl.BindVertexArray(vertex_array_object);

            gl.GenBuffers(1, &mut vertex_buffer);
            gl.BindBuffer(GL_ARRAY_BUFFER, vertex_buffer);
            gl.BufferData(
                GL_ARRAY_BUFFER,
                std::mem::size_of_val(&VERTICES).try_into().unwrap(),
                VERTICES.as_ptr() as _,
                GL_STATIC_DRAW,
            );

            gl.GenBuffers(1, &mut vbo_indices);
            gl.BindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_indices);
            gl.BufferData(
                GL_ELEMENT_ARRAY_BUFFER,
                std::mem::size_of_val(&INDICES).try_into().unwrap(),
                INDICES.as_ptr() as _,
                GL_STATIC_DRAW,
            );

            let position_attribute_location =
                shader.attribute_location("a_position").try_into().unwrap();

            let texture_coord_attribute_location =
                shader.attribute_location("a_texcoord").try_into().unwrap();

            gl.VertexAttribPointer(
                position_attribute_location,
                3,
                GL_FLOAT,
                GLboolean::FALSE,
                std::mem::size_of::<[GLfloat; 5]>().try_into().unwrap(),
                std::mem::size_of::<[GLfloat; 0]>() as _,
            );

            gl.VertexAttribPointer(
                texture_coord_attribute_location,
                2,
                GL_FLOAT,
                GLboolean::FALSE,
                std::mem::size_of::<[GLfloat; 5]>().try_into().unwrap(),
                std::mem::size_of::<[GLfloat; 3]>() as _,
            );

            gl.EnableVertexAttribArray(position_attribute_location);
            gl.EnableVertexAttribArray(texture_coord_attribute_location);

            gl.BindVertexArray(0);
        };

        *self.state.lock().unwrap() = Some(State {
            gl,
            shader,
            fbo,
            in_info,
            out_info,
            vertex_array_object,
            vertex_buffer,
            vbo_indices,
        });

        Ok(())
    }

    fn gl_stop(&self) {
        *self.state.lock().unwrap() = None;
    }
}

impl BaseTransformImpl for SimpleGLTransform {
    const MODE: BaseTransformMode = BaseTransformMode::NeverInPlace;
    const PASSTHROUGH_ON_SAME_CAPS: bool = false;
    const TRANSFORM_IP_ON_PASSTHROUGH: bool = false;

    fn transform(&self, inbuf: &Buffer, outbuf: &mut BufferRef) -> Result<FlowSuccess, FlowError> {
        let context = self.obj().gl_context().unwrap();
        if let Some(inbuf_sync_meta_api) = inbuf.meta::<GLSyncMeta>() {
            inbuf_sync_meta_api.wait(&context);
        }

        context.thread_add(|_| {
            let state = self.state.lock().unwrap();
            let State {
                shader,
                fbo,
                in_info,
                out_info,
                gl,
                vertex_buffer,
                vertex_array_object,
                vbo_indices,
            } = state.as_ref().unwrap();

            let in_frame =
                VideoFrame::<ReadableGL>::from_buffer_ref_readable_gl(inbuf, in_info).unwrap();

            let out_frame =
                VideoFrame::<WritableGL>::from_buffer_ref_writable_gl(outbuf, out_info).unwrap();

            let in_mem = in_frame.memory_gl(0).unwrap();
            let out_mem = out_frame.memory_gl_mut(0).unwrap();

            shader.use_();
            shader.set_uniform_1i("tex", 0);
            shader.set_uniform_1f("width", out_mem.texture_width() as f32);
            shader.set_uniform_1f("height", out_mem.texture_height() as f32);

            fbo.draw_to_texture(out_mem, || {
                unsafe {
                    gl.ActiveTexture(GL_TEXTURE0);
                    gl.BindTexture(in_mem.texture_target().to_gl(), in_mem.texture_id());

                    gl.BindVertexArray(*vertex_array_object);

                    gl.BindBuffer(GL_ELEMENT_ARRAY_BUFFER, *vbo_indices);
                    gl.BindBuffer(GL_ARRAY_BUFFER, *vertex_buffer);

                    gl.DrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_SHORT, 0 as _);

                    gl.BindVertexArray(0);
                }
                true
            });
        });

        if let Some(outbuf_sync_meta_api) = outbuf.meta::<GLSyncMeta>() {
            outbuf_sync_meta_api.set_sync_point(&context);
        }

        Ok(FlowSuccess::Ok)
    }
}

impl ElementImpl for SimpleGLTransform {
    fn metadata() -> Option<&'static ElementMetadata> {
        static ELEMENT_METADATA: Lazy<ElementMetadata> = Lazy::new(|| {
            ElementMetadata::new(
                "SimpleGLTransform",
                "Filter/Effect/Video",
                "Simple GL identity transform",
                "Anders Hellerup Madsen <ahem@github.com>",
            )
        });

        Some(&*ELEMENT_METADATA)
    }

    fn pad_templates() -> &'static [PadTemplate] {
        static PAD_TEMPLATES: Lazy<Vec<PadTemplate>> = Lazy::new(|| {
            let sink_pad_template = PadTemplate::new(
                "sink",
                PadDirection::Sink,
                PadPresence::Always,
                &Caps::from_str("video/x-raw(memory:GLMemory),format=RGBA").unwrap(),
            )
            .unwrap();

            let src_pad_template = PadTemplate::new(
                "src",
                PadDirection::Src,
                PadPresence::Always,
                &Caps::from_str("video/x-raw(memory:GLMemory),format=RGBA").unwrap(),
            )
            .unwrap();

            vec![sink_pad_template, src_pad_template]
        });
        PAD_TEMPLATES.as_ref()
    }
}

impl GstObjectImpl for SimpleGLTransform {}

impl ObjectImpl for SimpleGLTransform {}

#[glib::object_subclass]
impl ObjectSubclass for SimpleGLTransform {
    const NAME: &'static str = "SimpleGLTransform";
    type Type = super::SimpleGLTransform;
    type ParentType = gst_gl::GLBaseFilter;

    fn with_class(_klass: &Self::Class) -> Self {
        Self {
            state: Mutex::new(None),
        }
    }
}

#[rustfmt::skip]
 const VERTICES: [GLfloat; 20] = [
     // coord (x, y, z), texture (u, v)
     -1.0, -1.0, 0.0, 0.0, 0.0,
      1.0, -1.0, 0.0, 1.0, 0.0,
      1.0,  1.0, 0.0, 1.0, 1.0,
     -1.0,  1.0, 0.0, 0.0, 1.0
 ];

const INDICES: [GLushort; 6] = [0, 1, 2, 0, 2, 3];
