use gst::glib;
use gst::prelude::*;
use once_cell::sync::Lazy;

mod imp;

static CAT: Lazy<gst::DebugCategory> = Lazy::new(|| {
    gst::DebugCategory::new(
        "simplegltransform",
        gst::DebugColorFlags::empty(),
        Some("Veo Seek util"),
    )
});

glib::wrapper! {
    pub struct SimpleGLTransform(ObjectSubclass<imp::SimpleGLTransform>) @extends gst_gl::GLBaseFilter, gst_base::BaseTransform, gst::Element, gst::Object;
}

unsafe impl Send for SimpleGLTransform {}
unsafe impl Sync for SimpleGLTransform {}
