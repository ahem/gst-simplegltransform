Build with:

```
$ cargo build
```

Example of using the example plugin in a pipeline:

```
$ GST_PLUGIN_PATH=target gst-launch-1.0 gltestsrc ! simplegltransform ! autovideosink
```
